core = 7.x
api = 2
projects[drupal][version] = "7.103"


; Modules
projects[apachesolr][version] = "1.8"

projects[apachesolr_multisitesearch][version] = "1.1"

projects[comment_notify][version] = "1.2"

projects[ctools][version] = "1.12"

projects[date][version] = "2.8"

projects[diff][version] = "3.2"

projects[election][version] = "1.0"
; Drag & drop voting
; https://www.drupal.org/node/2682865#comment-11968759
projects[election][patch][] = "https://www.drupal.org/files/issues/draganddropballotsorting-2682865-10.patch"

projects[email][version] = "1.3"

projects[entity][version] = "1.9"

projects[features][version] = "2.11"

projects[field_extrawidgets][version] = "1.1"

projects[google_analytics][version] = "2.6"

projects[link][version] = "1.11"

projects[name][version] = "1.9"

projects[pathauto][version] = "1.3"

projects[redirect][version] = "1.0-rc3"

projects[r4032login][version] = "1.8"

projects[reply][version] = "2.x-dev"
projects[reply][download][revision] = "11a9911"
; Template preprocess variables are not available when reply.tpl.php is overridden in a
; theme.
; https://www.drupal.org/node/2403805#comment-9512465
projects[reply][patch][] = "https://www.drupal.org/files/issues/2403805-1.patch"

projects[rules][version] = "2.6"

projects[strongarm][version] = "2.0"

projects[tag1_d7es][version] = "1.2"

projects[token][version] = "1.5"

projects[twitterfield][version] = "1.0-rc1"

projects[views][version] = "3.21"

projects[views_bulk_operations][version] = "3.3"
projects[views_datasource][version] = "1.0-alpha2"

projects[views_data_export][version] = "3.1"

projects[votingapi][version] = "2.6"

projects[webform][version] = "4.21"


; Custom modules
projects[association_drupalorg][version] = "1.x-dev"
projects[association_drupalorg][download][revision] = "e5755c9"


; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"
